package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.lang.Math;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
   
    
    public void run() {
        
        while(true) {
           
            System.out.print(" Let's play round" + " "+(roundCounter++) + "\n ");
            String myMove = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            

            // Sjekke om hva bruker fikk går ann
            while(!myMove.equals("rock") && !myMove.equals("paper") && !myMove.equals("scissors")){
                System.out.println("I don't understand" +" " + myMove + " " + "." + "Try agian");
                //System.out.println(readInput("Your choice (rock/paper/scissors)? ").toLowerCase());
                myMove = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            
            } 
                
                //Run random for computer 
                int rand = (int)(Math.random() * 3);
                String opponetMove = ""; 
                if (rand == 0) {
                    opponetMove = rpsChoices.get(0); //"rock"
                } else if(rand == 1) {
                    opponetMove = rpsChoices.get(1); //"paper"
                } else { 
                    opponetMove = rpsChoices.get(2); //"scissors"
                }
                
                String choice =" Human chose "+ opponetMove + ", computer chose "  +myMove + ". "; 

                //Om det er uavgjort
                if(myMove.equals(opponetMove)){
                    System.out.println(choice + "It's a tie!");
                }else if((myMove.equals("rock") && opponetMove.equals("scissors")) || (myMove.equals("scissors")  && opponetMove.equals("paper")) || (myMove.equals("paper") && opponetMove.equals("rock"))) {
                    System.out.println(choice + "Human wins!");
                    humanScore += 1;
                }else {
                    System.out.println(choice +"Computer lose!");
                    computerScore += 1;
                }
            

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            String input = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    
            while(!input.equalsIgnoreCase("n") && !input.equalsIgnoreCase("y")){
                System.out.println("I don't understand" +  " " + input + ". " +" "+ "Try again" );
                input = readInput("Do you wish to continue playing? (y/n)? ").toLowerCase();
                

            } 
            if(input.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
